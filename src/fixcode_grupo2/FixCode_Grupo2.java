/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package fixcode_grupo2;

import java.util.Scanner;

/**
 *
 * @author vitor
 */
public class FixCode_Grupo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int num;
        Scanner ler = new Scanner(System.in);
        
        do {
            System.out.println("Introduza um número de 0 - 9");
            num = ler.nextInt();   
        }while (num < 0 || num > 9);
        
        switch (num) {
        
            case 0:
                System.out.println("Zero");
                break;
            case 1:
                System.out.println("Um");
                break;
            case 2:
                System.out.println("Dois");
                break;
            case 3:
                System.out.println("Três");
                break;
            case 4:
                System.out.println("Quatro");
                break;
            case 5:
                System.out.println("Cinco");
                break;
            case 6:
                System.out.println("Seis");
                break;
            case 7:
                System.out.println("Sete");
                break;
            case 8:
                System.out.println("Oite");
                break;
            case 9:
                System.out.println("Nove");
                break;
        }
    }
    
}
